package com.example.flowm3.core.export_feature.presentation

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.flowm3.core.Resource
import com.example.flowm3.core.export_feature.domain.model.ExportModel
import com.example.flowm3.core.export_feature.domain.repository.ExportRepository
import com.example.flowm3.core.export_feature.presentation.state.FileExportState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlin.random.Random
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

@HiltViewModel
class ExportViewModel @Inject constructor(
    private val exportRepository: ExportRepository
) : ViewModel() {
    private var collectingJob: Job? = null

    private var exportList = mutableListOf<ExportModel>()

    var collectedDataAmount by mutableStateOf(0)
        private set

    var fileExportState by mutableStateOf(FileExportState())
        private set

    fun generateExportFile() {
        collectingJob?.cancel()
        fileExportState = fileExportState.copy(isGeneratingLoading = true)
        exportRepository.startExportData(
            exportList.toList()
        ).onEach { pathInfo ->
            when (pathInfo) {
                is Resource.Loading -> {
                    pathInfo.data?.let {
                        fileExportState = fileExportState.copy(
                            generatingProgress = pathInfo.data.progressPercentage
                        )
                    }
                }

                is Resource.Success -> {
                    fileExportState = fileExportState.copy(
                        isSharedDataReady = true,
                        isGeneratingLoading = false,
                        shareDataUri = pathInfo.data.path,
                        generatingProgress = 100
                    )
                }

                is Resource.Error -> {
                    fileExportState = fileExportState.copy(
                        failedGenerating = true,
                        isGeneratingLoading = false
                    )
                }
            }
        }.launchIn(viewModelScope)
    }

    fun onShareDataClicked() {
        fileExportState = fileExportState.copy(isSharedDataClicked = true)
    }

    fun onShareDataOpen() {
        fileExportState = fileExportState.copy(isSharedDataClicked = false)
    }

    init {
        collectingJob = viewModelScope.launch {
            while (true) {
                delay(2)
                collectedDataAmount += 160
                exportList.addAll(
                    listOf(
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis()),
                        ExportModel(Random.nextFloat() * 10000, System.currentTimeMillis())
                    )
                )
            }
        }
    }
}