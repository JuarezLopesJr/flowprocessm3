package com.example.flowm3.core.export_feature.data.converter

import com.example.flowm3.core.Resource
import com.example.flowm3.core.export_feature.domain.model.ExportModel
import kotlinx.coroutines.flow.Flow

interface FileDataConverter {
    fun convertSensorData(exportDataList: List<ExportModel>): Flow<Resource<GenerateInfo>>
}