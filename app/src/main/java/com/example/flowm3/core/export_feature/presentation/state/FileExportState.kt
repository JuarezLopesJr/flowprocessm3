package com.example.flowm3.core.export_feature.presentation.state

data class FileExportState(
    val isGeneratingLoading: Boolean = false,
    val isSharedDataClicked: Boolean = false,
    val isSharedDataReady: Boolean = false,
    val shareDataUri: String? = null,
    val failedGenerating: Boolean = false,
    val generatingProgress: Int = 0
)