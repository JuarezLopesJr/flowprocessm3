package com.example.flowm3.core.export_feature.data.converter

data class GenerateInfo(
    val byteArray: ByteArray? = null,
    val progressPercentage: Int = 0,
)