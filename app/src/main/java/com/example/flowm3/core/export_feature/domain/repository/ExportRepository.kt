package com.example.flowm3.core.export_feature.domain.repository

import com.example.flowm3.core.Resource
import com.example.flowm3.core.export_feature.domain.model.ExportModel
import com.example.flowm3.core.export_feature.domain.model.PathInfo
import kotlinx.coroutines.flow.Flow

interface ExportRepository {
    fun startExportData(exportList: List<ExportModel>): Flow<Resource<PathInfo>>
}