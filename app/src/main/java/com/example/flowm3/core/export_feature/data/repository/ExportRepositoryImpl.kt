package com.example.flowm3.core.export_feature.data.repository

import com.example.flowm3.core.Resource
import com.example.flowm3.core.export_feature.data.converter.FileDataConverter
import com.example.flowm3.core.export_feature.data.file.FileWriter
import com.example.flowm3.core.export_feature.domain.model.ExportModel
import com.example.flowm3.core.export_feature.domain.model.PathInfo
import com.example.flowm3.core.export_feature.domain.repository.ExportRepository
import javax.inject.Inject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map

class ExportRepositoryImpl @Inject constructor(
    private val fileWriter: FileWriter,
    private val fileDataConverter: FileDataConverter
) : ExportRepository {
    override fun startExportData(exportList: List<ExportModel>): Flow<Resource<PathInfo>> =
        fileDataConverter.convertSensorData(exportList).map {
            when (it) {
                is Resource.Loading -> {
                    return@map Resource.Loading(
                        PathInfo(
                            progressPercentage = it.data?.progressPercentage ?: 0
                        )
                    )
                }

                is Resource.Success -> {
                    it.data.byteArray?.let { byteArray ->
                        when (val result = fileWriter.writeFile(byteArray)) {
                            is Resource.Loading -> {
                                return@map Resource
                                    .Error("Unknown error...How did get here ?")
                            }

                            is Resource.Success -> {
                                return@map Resource.Success(
                                    PathInfo(
                                        path = result.data,
                                        progressPercentage = 100
                                    )
                                )
                            }

                            is Resource.Error -> {
                                return@map Resource.Error(result.errorMessage)
                            }
                        }
                    } ?: return@map Resource
                        .Error("Unknown error...How did get here ?")
                }

                is Resource.Error -> {
                    return@map Resource.Error(it.errorMessage)
                }
            }
        }.flowOn(Dispatchers.IO)
}