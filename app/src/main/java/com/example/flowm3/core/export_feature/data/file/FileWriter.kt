package com.example.flowm3.core.export_feature.data.file

import com.example.flowm3.core.Resource

interface FileWriter {
    suspend fun writeFile(byteArray: ByteArray): Resource<String>

    companion object {
        const val FILE_NAME = "file_name"
    }
}