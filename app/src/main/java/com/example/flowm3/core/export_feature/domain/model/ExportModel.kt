package com.example.flowm3.core.export_feature.domain.model

data class ExportModel(
    val sensorData: Float,
    val time: Long,
)