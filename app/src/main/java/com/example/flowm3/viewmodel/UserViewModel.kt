package com.example.flowm3.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.flowm3.data.converter.DataProgressInfo
import com.example.flowm3.data.converter.UserRepository
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class UserViewModel(
    private val userRepository: UserRepository = UserRepository()
) : ViewModel() {

    private val userData = mutableListOf<Float>()

    var progressState by mutableStateOf(0)
        private set

    var isLoading by mutableStateOf(false)
        private set

    fun startGenerate() {
        isLoading = true
        viewModelScope.launch {
            userRepository.generateUserData(userData).map {
                DataProgressInfo(it.progressPercentage * 10)
            }.map {
                it.progressPercentage / 10
            }.collect {
                progressState = it
            }

            isLoading = false
            progressState = 0
        }
    }

    init {
        for (i in 0..1000000) {
            userData.add(i * 2f)
        }
    }
}