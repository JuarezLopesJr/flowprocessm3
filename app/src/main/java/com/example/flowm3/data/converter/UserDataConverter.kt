package com.example.flowm3.data.converter

import kotlin.math.pow
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

/* this class could be a interface */
class UserDataConverter {
    fun convertUserData(userData: List<Float>): Flow<DataConvertingInfo> = flow {
        var alreadyConvertedValues = 0

        userData.forEach { _ ->
            for (i in alreadyConvertedValues..1000) {
                val j = i.toDouble().pow(2.0)
            }

            emit(DataConvertingInfo(convertedDataAmount = ++alreadyConvertedValues))
        }
    }
}