package com.example.flowm3.data.converter

data class DataConvertingInfo(val convertedDataAmount: Int)