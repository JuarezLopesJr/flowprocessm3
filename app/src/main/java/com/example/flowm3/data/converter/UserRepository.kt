package com.example.flowm3.data.converter

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map

/* this class could be a interface and this parameter should be injected */
class UserRepository(
    private val userDataConverter: UserDataConverter = UserDataConverter()
) {
    fun generateUserData(userData: List<Float>): Flow<DataProgressInfo> {
        val valuesForOnePercent = userData.size / 100

        return userDataConverter.convertUserData(userData).filter {
            it.convertedDataAmount % valuesForOnePercent == 0
            /* using map to convert to the expected type return of Flow<DataProgressInfo> */
        }.map {
            DataProgressInfo(it.convertedDataAmount / valuesForOnePercent)
            /* using flowOn to change the flow context of the above flow NOT DOWNSTREAM */
        }.flowOn(Dispatchers.IO)
    }
}