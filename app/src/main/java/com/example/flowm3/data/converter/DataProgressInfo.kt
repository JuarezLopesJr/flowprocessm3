package com.example.flowm3.data.converter

data class DataProgressInfo(val progressPercentage: Int)