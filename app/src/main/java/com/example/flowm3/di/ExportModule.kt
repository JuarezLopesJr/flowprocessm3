package com.example.flowm3.di

import android.content.Context
import com.example.flowm3.core.export_feature.data.converter.FileDataConverter
import com.example.flowm3.core.export_feature.data.converter.csv.DataConverterCSV
import com.example.flowm3.core.export_feature.data.file.AndroidInternalStorageFileWriter
import com.example.flowm3.core.export_feature.data.file.FileWriter
import com.example.flowm3.core.export_feature.data.repository.ExportRepositoryImpl
import com.example.flowm3.core.export_feature.domain.repository.ExportRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ExportModule {
    @Provides
    @Singleton
    fun provideFileWriter(@ApplicationContext context: Context): FileWriter {
        return AndroidInternalStorageFileWriter(context)
    }

    @Provides
    @Singleton
    fun provideFileDataConverter(): FileDataConverter {
        return DataConverterCSV()
    }

    @Provides
    @Singleton
    fun provideExportRepository(
        fileWriter: FileWriter,
        fileDataConverter: FileDataConverter
    ): ExportRepository {
        return ExportRepositoryImpl(fileWriter, fileDataConverter)
    }
}