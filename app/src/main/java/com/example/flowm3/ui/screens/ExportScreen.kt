package com.example.flowm3.ui.screens

import android.content.Intent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Share
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.flowm3.core.export_feature.presentation.ExportViewModel
import java.io.File

@Composable
fun ExportScreen(
    modifier: Modifier = Modifier,
    exportViewModel: ExportViewModel = hiltViewModel()
) {
    val fileExportState = exportViewModel.fileExportState
    val context = LocalContext.current

    LaunchedEffect(key1 = fileExportState) {
        if (fileExportState.isSharedDataClicked) {
            val uri = FileProvider.getUriForFile(
                context,
                context.applicationContext.packageName + ".provider",
                File(fileExportState.shareDataUri!!)
            )

            val intent = Intent(Intent.ACTION_SEND)

            intent.apply {
                type = "text/csv"
                putExtra(Intent.EXTRA_SUBJECT, "Export Data")
                putExtra(Intent.EXTRA_STREAM, uri)
            }

            val chooser = Intent.createChooser(intent, "Share with")

            ContextCompat.startActivity(
                context,
                chooser,
                null
            )

            exportViewModel.onShareDataOpen()
        }
    }

    Box(
        modifier = modifier
            .fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceAround
        ) {
            Text(
                text = "Collected data amount: ${exportViewModel.collectedDataAmount}",
                style = MaterialTheme.typography.titleLarge,
                fontWeight = FontWeight.Bold,
                textAlign = TextAlign.Center,
                color = MaterialTheme.colorScheme.onBackground
            )

            Button(
                onClick = exportViewModel::generateExportFile,
                colors = ButtonDefaults.buttonColors(
                    containerColor = Color.Red,
                    contentColor = Color.White,
                    disabledContainerColor = Color.LightGray
                ),
                modifier = Modifier
                    .fillMaxWidth(0.9f)
                    .padding(12.dp),
                shape = CircleShape,
                enabled = !fileExportState.isSharedDataReady
            ) {
                Text(
                    text = "Generate file",
                    style = MaterialTheme.typography.bodyMedium,
                    fontWeight = FontWeight.Bold,
                    textAlign = TextAlign.Center
                )
            }

            AnimatedVisibility(visible = fileExportState.isSharedDataReady) {
                IconButton(onClick = exportViewModel::onShareDataClicked) {
                    Icon(
                        imageVector = Icons.Default.Share,
                        contentDescription = null,
                        tint = Color.Blue,
                        modifier = Modifier.size(120.dp),
                    )
                }
            }
        }
    }

    if (fileExportState.isGeneratingLoading) {
        Dialog(onDismissRequest = { }) {
            Column(
                modifier = Modifier.fillMaxWidth(),
                verticalArrangement = Arrangement.spacedBy(16.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                CircularProgressIndicator(color = Color.Green)

                Text(
                    text = "Generating file (${fileExportState.generatingProgress}%)...",
                    color = Color.White,
                    style = MaterialTheme.typography.headlineLarge,
                    fontWeight = FontWeight.Medium
                )
            }
        }
    }
}